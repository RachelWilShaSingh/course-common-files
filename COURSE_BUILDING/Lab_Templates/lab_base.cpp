/* Do not modify this file */

#include <iostream>     // use cout and cin
#include <string>       // use strings
using namespace std;    // using the C++ standard library

#include "student_code.hpp"
#include "lab_menu.hpp"

int main()
{
    RunLab( "Lab 1", YOUR_NAME );
    
    return 0;
}


