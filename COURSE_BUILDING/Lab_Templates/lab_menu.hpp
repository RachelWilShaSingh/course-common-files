/* Do not modify this file */

#ifndef _LAB_MENU_HPP
#define _LAB_MENU_HPP

#include <vector>       // For my menu list stuff
#include <limits>       // For my menu list stuff
#include <iostream>     // use cout and cin
#include <string>       // use strings

using namespace std;    // using the C++ standard library


void Clear()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        cin.ignore();
        cin.get();
        // cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    #endif
}

void DrawHorizontalBar( int width )
{
    for ( int i = 0; i < width; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Header( const string& header )
{
    DrawHorizontalBar( 80 );
    string head = "| " + header + " |";
    cout << " " << head << endl << " ";
    DrawHorizontalBar( head.size() );
    cout << endl;
}

void ShowMenu( const vector<string> options )
{
    for ( unsigned int i = 0; i < options.size(); i++ )
    {
        cout << " " << (i+1) << ".\t" << options[i] << endl;
    }
}

int GetValidChoice( int min, int max, const string& message )
{
    if ( message != "" )
    {
        cout << endl;
        DrawHorizontalBar( message.size() + 2 );
        cout << " " << message << endl;
    }

    int choice;
    cout << "  >> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid selection. Try again." << endl;
        cout << "  >> ";
        cin >> choice;
    }

    return choice;
}

void RunLab( const string& labName, const string& studentName )
{
    while ( true )
    {
        Clear();

        Header( labName + " - " + studentName );
        ShowMenu( { "LabProgram1", "LabProgram2", "LabProgram3", "Exit" } );
        int runProgram = GetValidChoice( 1, 4, "Run which program?" );

        Clear();

        if        ( runProgram == 4 )
        {
            break;
        }
        
        else if   ( runProgram == 1 )
        {
            Header( "Program 1" );
            LabProgram1();
        }
            
        else if ( runProgram == 2 )
        {
            Header( "Program 2" );
            LabProgram2();
        }
            
        else if ( runProgram == 3 )
        {
            Header( "Program 3" );
            LabProgram3();
        }
        
        Pause();
    }
}


#endif
