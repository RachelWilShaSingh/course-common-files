# Topic Review: Data Types and Variables

## 1. Data types and their values

<table>
<tr>
<th>Data Type</th>
<th>Description</th>
<th>Example code</th>
</tr>

<tr>
<td>Integer</td>
<td>Whole numbers</td>
<td>
<pre>
int a = 1;
int b = -50;
int c = 100;
</pre>
</td>
</tr>

<tr>
<td>Float, Double</td>
<td>Numbers with a decimal point</td>
<td>
<pre>
float cost = 9.99;
double ratio = 2.0;
</pre>
</td>
</tr>

<tr>
<td>Boolean</td>
<td>True or False; can also be assigned integers in C++</td>
<td>
<pre>
bool isDone = true;
bool isSaved = false;
bool sameValue = 0; // false
bool diffValue = 1; // true
</pre>
</td>
</tr>

<tr>
<td>String</td>
<td>Values stored within double-quotes; any length</td>
<td>
<pre>
string state = "Kansas";
string input = "20";
</pre>
</td>
</tr>

<tr>
<td>Character</td>
<td>Single symbols (characters) stored within single-quotes</td>
<td>
<pre>
char currency = '$';
char choice = 'y';
char menuItem = '1';
</pre>
</td>
</tr>

</table>

## 2. Variable declaration

To use a variable in C++, first you must **declare** it
prior to usage.

A variable declaration contains:

1. Data type
2. Variable name
3. Initial value (optional)

If you declare a variable without initializing it, it will look like this:

```c++
int number;
```

If you declare a variable and initialize it, it will look like this:

```c++
string title = "Exam1";
```

You can also assign multiple variables on the same line of code, separated by commas. If you do this, the variables will be the same data-type:

```c++
string student1, student2, student3;
```

**Garbage:** If you do not initialize a variable, in C++ it will default to garbage. Garbage is random data from elseware in the system, and you cannot rely on what it will be. Make sure to initialize your variables before you use them!

## 3. Variable usage

If a variable is already declared, you can use it throughout your program. You do not need to specify its data type when you use it - that is only for the declaration.

```c++
int a = 2; // declaration
int b = 3; // declaration

cout << a << endl; // output value of a
cout << b << endl; // output value of b
cout << a + b << endl; // output the value of (a+b) added together
```

If you do an operation on a variable name, that operation will be done with the *contents* (value) of the variable. With the above example, when it sees "a", 2 will be used, and when it sees "b", 3 will be used.  When *a+b* is outputted, it will calculate *2+3* and display 5 to the screen.

### Strings

Once a string variable is declared, you can store values in the string, as long as the data is in double-quotes. Then, you just use the variable name to access that data.

```c++
string name = "Bob";
cout << name << endl;
```

**Common error:** If you're assigning one string variable the value of another string variable, neither *variable name* will be in double quotes.

```c++
string spring = "SPRING";
string fall = "FALL";

string semester = spring;

cout << "Semester: " << semester << endl;
```

The above code will display:

	Semester: SPRING


## 4. Variable assignment

When assigning a *value* to a variable, we use the **assignment operator**, =.

* The **variable being given a value** goes on the **left-hand side (LHS)** of the equal sign.
* The **value being assigned to the variable** goes on the **right-hand side (RHS)** of the equal sign.

You can assign a variable some value in several ways...:

* Hard-coding: Directly assigning a value, such as 2, 'a', or "Value".
* Assigning one variable the value of a second variable.
* Assigning a variable the result of some operation. (e.g., a math formula)
* Assigning a variable the input from the keyboard, typed by the user.

Examples:

```c++
int a, b, temp, diff1, diff2;

// Directly assign value to variable
a = 1;
b = 2;

// Calculate difference
diff1 = a - b;

// Swap the values
temp = a;
a = b;
b = temp;

// Calculate the difference again
diff2 = a - b;

// Display results to the screen
cout << diff1 << endl;
cout << diff2 << endl;
```

**Coding logic:** In the above code, why is the variable *temp* assigned the value of *a*? What's the point of this *temp* variable?

