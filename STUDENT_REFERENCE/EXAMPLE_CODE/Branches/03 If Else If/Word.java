import java.util.Scanner;

public class Word
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );
        
        System.out.print( "Enter a word: " );

        String word = input.next();

        System.out.println( "\n What to change?" );
        System.out.println( "\t 1. Change to upper case" );
        System.out.println( "\t 2. Change to lower case" );
        System.out.println( "\t 3. Replace 'a' with 'A'" );

        int choice = input.nextInt();

        if ( choice == 1 )
        {
            word = word.toUpperCase();
        }
        else if ( choice == 2 )
        {
            word = word.toLowerCase();
        }
        else if ( choice == 3 )
        {
            word = word.replace( 'a', 'A' );
        }

        System.out.println( "\n Result: " + word );
    }
}
