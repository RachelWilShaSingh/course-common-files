import java.util.Scanner;

public class Bank
{
    static Scanner input = new Scanner( System.in );
    static double balance;

    public static void DisplayMainMenu()
    {
        System.out.println();
        System.out.println( "--------------- BANK ---------------" );
        System.out.println( "1. Deposit" );
        System.out.println( "2. Withdraw" );
        System.out.println( "3. Exit" );
        System.out.println();
        System.out.println( "Balance: $" + balance );
        System.out.println();        
    }

    public static int GetChoice( int min, int max )
    {
        int choice;
        System.out.print( "Please enter a number between " + min + " and " + max + ": " );
        choice = input.nextInt();

        while ( choice < min || choice > max )
        {
            System.out.println( "Invalid input, try again: " );
            choice = input.nextInt();
        }
        return choice;
    }

    public static void Deposit()
    {
        System.out.print( "Deposit how much? ... $" );
        double amount = input.nextDouble();

        if ( amount <= 0 )
        {
            System.out.println( "Invalid amount; can't be 0 or less" );
        }
        else
        {
            balance += amount;
            System.out.println( "$" + amount + " added to balance" );
        }
    }

    public static void Withdraw()
    {
        System.out.print( "Withdraw how much? ... $" );
        double amount = input.nextDouble();
        
        if ( amount <= 0 )
        {
            System.out.println( "Invalid amount; can't be 0 or less" );
        }
        else if ( amount > balance )
        {
            System.out.println( "Invalid amount; can't be more than the balance" );
        }
        else
        {
            balance += amount;
            System.out.println( "$" + amount + " added to balance" );
        }
    }
    
    public static void main( String args[] )
    {
        boolean quit = false;
        balance = 0.0;

        while ( quit == false )
        {
            DisplayMainMenu();
            int choice = GetChoice( 1, 3 );
            System.out.println();

            switch( choice )
            {
                case 1: Deposit(); break;
                case 2: Withdraw(); break;
                case 3: quit = true; break;
            }
        }
    }
}
