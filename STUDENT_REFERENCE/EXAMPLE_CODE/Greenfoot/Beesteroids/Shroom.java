import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Shroom extends Actor
{
    public Shroom()
    {
        int angle = Greenfoot.getRandomNumber( 360 );
        turn( angle );
    }
    
    public void act() 
    {
        move( 1 );
        DetectCollision();
    } 
    
    public void DetectCollision()
    {
        if ( isTouching( Player.class ) || isTouching( Bullet.class ) )
        {
            getWorld().removeObject( this );
            return;
        }
    }   
}
