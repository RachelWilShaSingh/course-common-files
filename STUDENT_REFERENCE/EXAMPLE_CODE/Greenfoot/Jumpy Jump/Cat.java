import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Cat extends Actor
{
    float velocityX, velocityY;
    int gravity, jumpSpeed, speed;
    int jumpCounter;
    int score;
    
    public Cat()
    {
        velocityY = 0;
        velocityX = 0;
        gravity = 5;
        jumpSpeed = -7;
        speed = 5;
        jumpCounter = 0;
        score = 0;
    }
    
    public void act() 
    {
        if ( isTouching( Tile.class ) )
        {
            // Touching the floor
            velocityY = 0;
            jumpCounter = 0;
        }
        else if ( jumpCounter == 0 )
        {
            velocityY = gravity;
        }
        
        if ( velocityX > 5 ) { velocityX = 5; }
        if ( velocityX < -5 ) { velocityX = -5; }
        
        if ( jumpCounter > 0 )
        {
            jumpCounter--;
        }
        
        getWorld().showText( "Points: " + score, 70, 20 );
        
        handleKeyboard();
        adjustPosition();
        checkCoin();
    }   
    
    public void checkCoin()
    {
        Coin colliding = (Coin)getOneIntersectingObject( Coin.class );
        if ( colliding != null )
        {
            score += 5;
            Greenfoot.playSound( "coin.wav" );
            getWorld().removeObject( colliding );
        }
    }
    
    public void handleKeyboard()
    {
        if ( Greenfoot.isKeyDown( "up" ) && isTouching( Tile.class ) )
        {
            // Jump
            velocityY = jumpSpeed;
            jumpCounter = 15;
            Greenfoot.playSound( "jump.wav" );
        }
        
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            velocityX -= speed;
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            velocityX += speed;
        }
        else
        {
            // Deaccelerate
            if ( velocityX < 0 )
            {
                velocityX += 1;
            }
            else if ( velocityX > 0 )
            {
                velocityX -= 1;
            }
        }
        
        if ( Greenfoot.isKeyDown( "down" ) )
        {
            setImage( "catsit.png" );
        }
        else
        {
            setImage( "cat.png" );
        }
    }
    
    public void adjustPosition()
    {
        int x = getX();
        int y = getY();
        
        x += velocityX;
        y += velocityY;
        
        setLocation( x, y );
    }
}
