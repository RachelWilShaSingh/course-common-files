#include <iostream>
#include <string>
using namespace std;

double GetPlanCost(int data, int talk, int text)
{
	double price = 30;

	if (data == 1) { price += 5; }
	else if (data == 2) { price += 10; }
	else if (data == 3) { price += 15; }

	if (talk == 1) { price += 5; }
	else if (talk == 2) { price += 12; }
	else if (talk == 3) { price += 15; }

	if (text == 1) { price += 5; }
	else if (text == 2) { price += 10; }
	else if (text == 3) { price += 16; }

	return price;
}

void Test_GetPlanCost()
{
	int input_data_arr[4] = { 0, 1, 2, 3 };
	int input_talk_arr[4] = { 0, 0, 0, 0 };
	int input_text_arr[4] = { 0, 0, 0, 0 };
	double expected_output_arr[4] = { 30, 35, 40, 45 };
	double actual_output;

	// Test1
	for (int i = 0; i < 4; i++)
	{
		int input_data = input_data_arr[i];
		int input_talk = input_talk_arr[i];
		int input_text = input_text_arr[i];
		double expected_output = expected_output_arr[i];

		actual_output = GetPlanCost(input_data, input_talk, input_text);

		if (actual_output != expected_output)
		{
			cout << "Test " << i << " failed - Actual output was " << actual_output <<
				", but was expecting " << expected_output << endl;
		}
		else
		{
			cout << "Test " << i << " passed" << endl;
		}

	}
}

int main()
{
	Test_GetPlanCost();



	while (true);
	return 0;
}
