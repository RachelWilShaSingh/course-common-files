#include <iostream>
#include <string>
using namespace std;

int main()
{
    int number = 2;
    float price = 9.99;
    char letter = 'a';
    bool boolean = true;
    string name = "Bob";

    cout << number << endl;
    cout << price << endl;
    cout << letter << endl;
    cout << boolean << endl;
    cout << name << endl;
    
    return 0;   // exit program
}
