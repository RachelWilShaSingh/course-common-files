public class Java_Data_Types
{
    public static void main( String args[] )
    {
        int number = 2;
        double price = 9.99;
        char letter = 'a';
        boolean bool = true;
        String name = "Bob";

        System.out.println( number );
        System.out.println( price );
        System.out.println( letter );
        System.out.println( bool );
        System.out.println( name );
    }
}
