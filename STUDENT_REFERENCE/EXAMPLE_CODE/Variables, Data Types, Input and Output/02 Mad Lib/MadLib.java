import java.util.Scanner;

public class MadLib
{
    static Scanner input = new Scanner( System.in );
    
    public static void main( String args[] )
    {
        // Declare variables
        int number;
        double money;
        String noun, noun2;

        // Get user input for story
        System.out.print( "Enter a number: " );
        number = input.nextInt();

        System.out.print( "Enter a dollar amount: $" );
        money = input.nextDouble();

        System.out.print( "Enter a noun: " );
        noun = input.next();

        System.out.print( "Enter another noun (plural): " );
        noun2 = input.next();

        // Display story
        System.out.println( "BEST STORY EVER" );
        System.out.println( "Once there was a " + noun + " that had"  );
        System.out.println( number + " " + noun2 + " and $" + money );
        System.out.println();
        System.out.println( "The End" );
    }
}
