# Looping

Sometimes, we want to execute some commands multiple times.

Some examples of needing to loop are:

* Keep asking the user to re-enter their answer *while*
the choice they selected was outside of the valid option range.
* Keep processing work *while* work is in the work queue.
* Keep computing some formula *n* amount of times.
* Run the following code *n* times.

---

## While loops

A while loop is similar to an
[**if statement**](https://github.com/Rachels-Courses/Course-Common-Files/blob/organized/STUDENT_REFERENCE/GLOSSARY/04%20Branching.md#if-statements)
in that it looks at some
[**condition**](https://github.com/Rachels-Courses/Course-Common-Files/blob/organized/STUDENT_REFERENCE/GLOSSARY/04%20Branching.md#condition)
and will execute its internal code if that condition evaluates to **true**.

The difference, however, is that it will keep executing that same internal code
**while** the condition is **true**. It will only stop looping once
the condition finally evaluates to **false**.

![while loop diagram](images/while_loop.png)

If the condition ends up evaluating to **false** before the loop has ever run,
then the loop is skipped completely - just like an if statement. The
**while** loop only begins if the condition starts off as true.

```c++
char choice;
cout << "Enter choice (a, b, or c): ";
cin >> choice;

while ( choice != 'a' && choice != 'b' && choice != 'c' )
{
    // Invalid choice, ask again.
    cout << "Invalid choice, try again: ";
    cin >> choice;
}

// Have a valid choice, continue the program
```

---

## Do...While loops

A **Do...While** loop is very similar to a standard **while loop**,
except that a **do...while** loop **always runs at least one time.**

```java
System.out.println( "Options: 1. burger, 2. fries, 3. salad" );
int choice = 0;

do
{
    System.out.print( "Selection: " );
    choice = scanner.nextInt();
    
} while ( choice != 1 && choice != 2 && choice != 3 );
```

This way, you can write some code that will execute, and specify some
condition *afterward*.

If the condition evaluates to **false** by the end of the **do..while**
loop's contents, then the instructions will not be executed a 2nd time
and the program will continue.

Otherwise, if we hit the end of the **do...while** loop and the condition
evaluates to **true**, then we start the loop over from the beginning,
and it executes just like a **while loop** -- it will keep looping until
that condition eventually evaluates to **false**.

---

## Infinite loops

It is very easy to write an infinite loop using a **while** or **do...while** loop.

These loops work with conditions, and will keep looping while the condition
evaluates to **true**.

If there is nothing *within* the loop that will ever cause the condition
to evaluate to **false**, then we have an infinite loop and the internal code
will be executed *forever*.

```c++
int x = 10;

while ( x > 0 )
{
    DoAThing();
}
```

In this case, there is nothing that ever makes ```x <= 10```, so our
condition ```( x > 0 )``` will be true *forever*, and therefore the loop
will continue looping *forever*.

---

## For loops

A for loop is a useful type of loop if you want to do counting,
such as processing items *1 through 10*.

For loops contain three sections within their parenthesis:

```c++
for ( STARTING_CODE; LOOP_CONDITION; RUN_AFTER_EACH_LOOP )
{
}
```

* The **STARTING_CODE** is done at the beginning, before the loop starts.
We can use this to make a counter variable, or initialize some variable state.
* The **LOOP_CONDITION** is the same as with a while loop; the loop will continue
while this condition evaluates to true.
* The **RUN_AFTER_EACH_LOOP** is a small bit of code that will be executed
*after* each iteration of the loop, so we could adjust the counter variable each time...

Count up from 1 to 10:

```c++
for ( int i = 1; i <= 10; i++ )
{
    cout << i << endl;
}
```

Count down from 10 to 1:

```c++
for ( int i = 10; i >= 1; i-- )
{
    cout << i << endl;
}
```

---

## Break

Let's say that you encounter a situation where you need to leave the loop
immediately (maybe an error or some special case) - you can use the ```break```
command to leave any loop and continue the program flow.

```c++
while ( isProcessing )
{
    if ( foundError )
    {
        break; // leave the loop!
    }
    // ...
}
```

---

## Continue

If you want to continue running the loop, but find something that
you want to skip over, you can use ```continue``` to finish this iteration
of the loop and keep going.

```c++
while ( isProcessing )
{
    if ( foundError )
    {
        continue; // stop this iteration, continue at next iteration
    }
    // ...
}
```
