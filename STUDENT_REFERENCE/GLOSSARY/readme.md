# Glossary

Select one of the files above to read through the glossary
for a specific topic.

These will help you learn programming terminology (aka *jargon*),
which is important so that you can understand what the instructor,
your peers, and other programmers are saying to you.

Reference back to this glossary if you are having trouble understanding
parts of assignments.
