# Working with BlueJ

**Contents**

* [Creating the project](#creating-the-project)
* [Create a source file](#create-a-source-file)
* [Starter code](#starter-code)
* [Running the program](#running-the-program)

---

## Creating the project

To create a project in BlueJ, go to **Project > New Project...**

From the *New Project* box, navigate to where you want to put your new
project in.

Create a new folder (You can use the ![new folder](images/new_project.png) icon
or right-click to create a new folder) and create a folder. Its name will
be the name of your project.

Once you've named it, make sure it is selected and click **Create**.

![Click create button to make project](images/create_project.png)

It will start you off with an empty project, which looks like this:

![empty project view](images/empty_project.png)

---

## Create a source file

Before you can begin coding, you need to create a Class.

Click on the **New Class...** button and give your Class a name.
Click **Ok** once you're done.

![New class dialog box](images/new_class.png)

Then, right-click your Class (the orange box) and select **Open Editor**.

![Open Editor](images/right_click.png)

---

## Starter code

BlueJ will generate starter code for you, but a lot of it is unneeded.

You can instead just paste in the following code, but make sure to replace
**MyProgram** with the name of YOUR class.

```java
import java.util.Scanner;

public class MyProgram
{
    public static void main()
    {
    }
}
```

**main()** is a function where programs usually begin in.

---

## Running the program

Before you can run the program, you need to **compile**. Click on the
**Compile** button, either in the main BlueJ window or in your code editor.
Fix any syntax errors, if applicable. Then you can run your program.

When you're using the ```static void main``` function, you can run it
from BlueJ by right-clicking the class (the orange box) and selecting
the main function:

![Right-click the class and select main to run it](images/run_program.png)


