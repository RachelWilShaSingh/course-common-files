* [First time Git setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)
* [Getting help](https://git-scm.com/book/en/v2/Getting-Started-Getting-Help)
* [Getting a repository](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)
* [Record changes to the repository](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)
  * Tracking new files
  * Staging modified files
  * Ignoring files
  * Viewing staged and unstaged changes
  * Committing your changes
  * Removing files
  * Moving files
* [View commit history](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)
* [Undoing things](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things)
