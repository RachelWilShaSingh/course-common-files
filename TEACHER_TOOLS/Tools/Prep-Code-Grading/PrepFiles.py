import os

# This script will do the following:
#
# 1. Rename all the folders (based on D2L's grading scheme,
# it removes the begin and end part, and leaves just the names of the students),
# 2. Unzip all zip files in subfolders
# 3. Move all code files in each student's subfolder into the "root" subfolder
# (so I don't have to hunt through Visual Studio project files)
# 4. Run wdiff on all source files and generate a report

def MainMenu():
    all_option = 6

    while ( True ):
        os.system( "clear" )
        
        rootFolder = os.path.dirname(os.path.realpath(__file__))
        studentFolders = GetFolders( rootFolder )
        
        print( "-------------------------------------------" )
        print( "- OPTIONS ---------------------------------" )
        print( "-------------------------------------------" )
        print( " * Root folder: \"" + rootFolder + "\"\t" )
        print( " * " + str( len( studentFolders ) ) + " subfolder(s)" )
        print( "" )
        print( " 1. RENAME folders" )
        print( " 2. BUILD from subfolders" )
        print( " 3. DIFF files" )
        print( " 4. SUMMARIZE diffs" )
        print( " 5. UNZIP subfolders" )
        print( " 6. ALL" )
        print( " 7. Exit" )
        print( "" )
        operation = input( "Which would you like to do? " )

        if ( operation == 1 or operation == all_option ):
            studentFolders = RenameFolders( rootFolder )

        if ( operation == 2 or operation == all_option ):
            BuildProjects( rootFolder, studentFolders )

        #MoveSourceFiles( rootFolder, studentFolders ) # Need to fix this

        if ( operation == 3 or operation == all_option ):
            sourceFileList = GetListOfSourceFiles( rootFolder )
            DiffFiles( rootFolder, sourceFileList )

        if ( operation == 4 or operation == all_option ):
            diffPath = os.path.join( rootFolder, "0_DIFF_RESULTS" )
            SummarizeDiffs( diffPath )

        if ( operation == 5 or operation == all_option ):
            UnzipSubfolders( rootFolder, studentFolders )

        if ( operation == 7 ):
            exit()

def WaitForConfirm():
    c = "n"
    while ( c != "y" ):
        print( "" )
        c = raw_input( "Continue? (y/n): " )

def GetFolders( rootFolder ):
    studentFolders = list()
    
    for root, directories, files in os.walk( ".", topdown = False ):
        for name in directories:
            studentFolders.append( name )
    
    return studentFolders

def RenameFolders( rootFolder ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    print( "\n\n Rename folders..." )
    
    studentFolders = list()
    
    for root, directories, files in os.walk( ".", topdown = False ):
        for name in directories:

            if ( "-" not in name ):
                continue
            
            dashAt = name.find( " - " )
            strippedName = name[dashAt+3:]            
            dashAt = strippedName.find( " - " )            
            strippedName = strippedName[:dashAt]
            
            print( "\t Rename folder: " + name + " to " + strippedName )

            os.system( "mv '" + name + "' '" + strippedName + "'" )

            studentFolders.append( strippedName )

    WaitForConfirm()

    return studentFolders

def UnzipSubfolders( rootFolder, studentFolders ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    print( "\n\n Unzip subfolders..." )
    
    for folder in studentFolders:
        print( "\t" + folder )
        os.chdir( folder )
        os.system( "unzip *.zip -d ." )
        os.chdir( rootFolder )

    WaitForConfirm()

def MoveSourceFiles( rootFolder, studentFolders ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    print( "\n\n Move source files..." )

    for folder in studentFolders:
        path = os.path.join( rootFolder, folder )
        
        command = "find . -name '*.cpp' -exec cp {} '" + path + "' \;"
        
        print( "\t" + folder + ", path: " + path + ", command: " + command )

        os.system( command )
        
    WaitForConfirm()

def GetListOfSourceFiles( rootFolder ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    print( "\n\n Get list of source files..." )
    sourceFileList = list()
    
    for ext_root, ext_dir, ext_files in os.walk( ".", topdown = False ):
        for folder in ext_dir:

            pathbase = os.path.join( ext_root, folder )

            for in_root, in_dir, in_files in os.walk( folder, topdown = False ):

                for file in in_files:
                    if ( ".hpp" in file or ".h" in file or ".cpp" in file ):
                        path = os.path.join( pathbase, file )

                        fileinfo = {
                            "path" : path,
                            "folder" : folder,
                            "file" : file
                        }
                        
                        sourceFileList.append( fileinfo )

    WaitForConfirm()
            
    return sourceFileList

def DiffFiles( rootFolder, sourceFileList ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    print( "\n\n Diff source files..." )

    counter = 0

    os.system( "mkdir 0_DIFF_RESULTS" )

    for file1 in sourceFileList:

        # Skip MY files
        if ( "Tester" in file1["file"] or "main" in file1["file"] ):
            continue
        
        for file2 in sourceFileList:
            
            if ( file1["path"] == file2["path"] ):
                # Skip if same path
                continue
                
            if ( file1["file"] != file2["file"] ):
                # Skip if different file name
                continue

            counter = counter + 1

            #print( "\t Compare file1: " + file1["path"] + " with file2: " + file2["path"] )
            
            # Create an output status
            f1 = os.path.join( file1[ "folder" ].replace( "./", "" ), file1[ "file" ] )
            f2 = os.path.join( file2[ "folder" ].replace( "./", "" ), file2[ "file" ] )
            
            filename = "0_DIFF_RESULTS/"
            filename = filename + file1[ "folder" ] + "_" + file1[ "file" ]
            filename = filename + "-"
            filename = filename + file2[ "folder" ] + "_" + file2[ "file" ] + ".txt"

            # Diff both source files
            command = "wdiff -s '" + file1[ "path" ] + "' '" + file2[ "path" ] + "' > '" + filename + "'"

            print( command )
            
            #print( "File 1:  \t" + f1 )
            #print( "File 2:  \t" + f2 )
            #print( "Results: \t" + filename )
            #print( "Command: \t" + command )
            
            os.system( command )

    print( str(counter) + " diffs" )

    WaitForConfirm()

def SummarizeDiffs( diffPath ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    os.chdir( diffPath )
    os.system( "grep 'changed' * > ../RESULTS.TXT" )

    WaitForConfirm()

def BuildProjects( rootFolder, studentFolders ):
    os.system( "clear" )
    print( "-------------------------------------------" )
    print( "- BUILD PROJECT ---------------------------" )
    print( "-------------------------------------------" )
    
    for s in studentFolders:
        print( " * Subfolder: \"" + s + "\"" )
        outname = s.replace( " ", "-" )
        command = "g++ -std=c++11 \"" + s + "\"/*.cpp -o \"./" + outname + ".out\""
        print( " * Command: [" + command + "]" )
        print( "" )
        os.system( command )

    WaitForConfirm()

MainMenu()


