#include <iostream>
#include <string>
using namespace std;

int main()
{
  string a, b, c;
  
  cout << "Enter a: ";
  cin >> a;
  cout << "Enter b: ";
  cin >> b;
  cout << "Enter c: ";
  cin >> c;
  
  cout << endl;
  cout << "Result:" << endl;
  cout << "* a: " << a << endl;
  cout << "* b: " << b << endl;
  cout << "* c: " << c << endl;
  
  return 0;
}
